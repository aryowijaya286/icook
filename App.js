import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SplashScreen from './src/screens/SplashScreen';
import OnBoardingScreen from './src/screens/OnBoardingScreen';
import HomeScreen from './src/screens/HomeScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {black, green} from './src/constants/color';
import RecipeDetail from './src/components/RecipeDetail';
import Recipes from './src/screens/Recipes';
import RecipesDetail from './src/screens/RecipesDetail';
import ExploreDetail from './src/screens/ExploreDetail';


const Stack = createNativeStackNavigator();

const Tab = createBottomTabNavigator();

function BottomNavigator() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarInactiveTintColor: black,
        tabBarActiveTintColor: green,
        headerShown: false,
      }}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color}) => <Icon name="home" color={color} size={26} />,
        }}
      />
      <Tab.Screen
        name="Recipe List"
        component={Recipes}
        options={{
          tabBarLabel: 'Recipes',
          tabBarIcon: ({color}) => (
            <Icon name="clipboard-list-outline" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="SplashScreen"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="OnBoarding" component={OnBoardingScreen} />
        <Stack.Screen name="HomeScreen" component={BottomNavigator} />
        <Stack.Screen name="Recipes" component={Recipes} />
        <Stack.Screen name="RecipeDetail" component={RecipeDetail} />
        <Stack.Screen name="RecipesDetail" component={RecipesDetail} />
        <Stack.Screen name="ExploreDetail" component={ExploreDetail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
