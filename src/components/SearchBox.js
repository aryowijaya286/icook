import {View, Text, TextInput, StyleSheet, ViewStyle} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {black, grey} from '../constants/color';

interface ISearchBox {
  style?: ViewStyle;
}

export default function SearchBox({style}: ISearchBox) {
  return (
    <View style={[styles.flexRowCenter, styles.searchStyle, style]}>
      <Icon name="search" size={20} style={{marginRight: 10}} />
      <TextInput placeholder="Pencarian Resep" style={{paddingVertical: 10}} />
    </View>
  );
}

const styles = StyleSheet.create({
  flexRowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  searchStyle: {
    backgroundColor: grey,
    marginHorizontal: 24,
    paddingHorizontal: 16,
    paddingVertical: 5,
    borderRadius: 10,
    marginTop: 20,
    marginBottom: 20,
  },
});
