import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  ViewStyle,
} from 'react-native';
import React from 'react';
import {black, border, green, grey, white} from '../constants/color';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {ImgPngFuel4} from '../constants/images';

interface IRecipeCard {
  onPress?: () => void;
  index: Number;
  image?: String;
  title?: String;
  price?: String;
  style?: ViewStyle;
}

export default function RecipeCard(props: IRecipeCard) {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[styles.cardStyle, props.style]}>
      <Image
        source={props?.image ?? ImgPngFuel4}
        style={{width: '100%', resizeMode: 'contain', height: 74}}
      />
      <View style={{height: 31}} />
      <View>
        <Text>{props?.title ?? 'Organic Bananas'}</Text>
        <Text style={{color: black}}>Resep Tumis Tempe Buncis Udang.</Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            backgroundColor: green,
            padding: 14,
            borderRadius: 20,
          }}>
          <Icon name="chevron-right" size={25} color={white} />
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardStyle: {
    width: 175,
    height: 248,
    borderWidth: 1,
    borderColor: grey,
    padding: 15,
    borderRadius: 10,
    backgroundColor: white,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
});
