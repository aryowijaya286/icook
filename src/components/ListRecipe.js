import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {black, border, green, grey, white} from '../constants/color';
import {ImgPngFuel4} from '../constants/images';
import RecipeCard from './RecipeCard';

export default function ListRecipe({title, navigation}: any) {
  return (
    <>
      <View
        style={[
          styles.flexRowCenter,
          {justifyContent: 'space-between', paddingHorizontal: 24},
        ]}>
        <Text style={{fontSize: 24, fontWeight: '600', color:black}}>{title}</Text>
        <Text style={{fontSize: 16, fontWeight: '600', color: green}}>
          See all
        </Text>
      </View>

      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {Array.from(Array(9)).map((val, index) => {
          return (
            <RecipeCard
              key={index}
              onPress={() => navigation.navigate('RecipeDetail')}
              index={index}
              style={
                index === 0
                  ? {marginLeft: 24, marginRight: 15}
                  : {
                      marginRight: 15,
                    }
              }
            />
          );
        })}
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  flexRowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 20,
  },
  cardStyle: {
    width: 175,
    height: 248,
    borderWidth: 1,
    borderColor: border,
    marginRight: 15,
    padding: 15,
    borderRadius: 10,
    backgroundColor: white,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
});
