import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import {black, border, green, grey, grey_light} from '../constants/color';
import Accordion from '../components/Accordion';
import Button from '../components/Button';

const {width: SCREEN_WIDTH} = Dimensions.get('screen');

export default function RecipesDetail({navigation}) {
  const images = [
    'https://rasabunda.com/wp-content/uploads/2021/11/Tumis-Buncis-Udang.jpg',
    'https://cdn.pixabay.com/photo/2017/06/02/18/24/watermelon-2367029_960_720.jpg',
  ];

  return (
    <>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <MaterialIcons name="chevron-left" size={25} />
        </TouchableOpacity>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{paddingBottom: 30}}>
        <View style={{position: 'relative', width: '100%'}}>
          <ScrollView
            scrollEventThrottle={16}
            horizontal={true}
            pagingEnabled
            showsHorizontalScrollIndicator={false}>
            {images.map((val, index) => (
              <View
                key={index}
                style={{
                  width: SCREEN_WIDTH,
                  height: 371,
                }}>
                <Image
                  source={{uri: val}}
                  style={{
                    height: 371,
                    width: '100%',
                    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,
                  }}
                />
              </View>
            ))}
          </ScrollView>
        </View>
        <View style={{paddingHorizontal: 24, marginTop: 30}}>
          <View style={styles.flexRowBetween}>
            <Text style={{fontSize: 24, fontWeight: 'bold', color:"#383838"}}>
              Resep Tumis Tempe Buncis Udang
            </Text>
            <MaterialCommunity name="heart-outline" size={24} color={grey} />
          </View>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: black,
              marginTop: 6,
            }}>
            Buatan : Novi Januarsi
          </Text>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: black,
              marginTop: 6,
            }}>
               @novijanuarsi_
          </Text>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: black,
              marginTop: 6,
            }}>
            Jakarta, Jakarta, Indonesia
          </Text>
          <View style={[styles.flexRowBetween, {marginVertical: 30}]}>
            <View style={styles.row}>
            </View>
          </View>
          <Accordion title="Bahan - Bahan">
          <Text style={{color:"#383838"}}>- 250 gr udang, kupas kulit</Text>
          <Text style={{color:"#383838"}}>- 250 gr buncis, potong2</Text>
          <Text style={{color:"#383838"}}>- 300 gr tempe potong dadu, goreng</Text>
          <Text style={{color:"#383838"}}>- 5 buah bawang merah </Text>
          <Text style={{color:"#383838"}}>- 4 siung bawang putih</Text>
          <Text style={{color:"#383838"}}>- 3 bh cabai merah besar</Text>
          <Text style={{color:"#383838"}}>- 3 bh cabai hijau besar</Text>
          <Text style={{color:"#383838"}}>- 2 cm lengkuas, memarkan</Text>
          <Text style={{color:"#383838"}}>- 1 btg daun bawang, iris serong</Text>
          <Text style={{color:"#383838"}}>- Bumbu Saus</Text>
          <Text style={{color:"#383838"}}>- 4 sdm kecap manis</Text>
          <Text style={{color:"#383838"}}>- Secukupnya kaldu jamur, garam, lada bubuk dan gula pasir</Text>
          <Text style={{color:"#383838"}}>- 100 ml air</Text>
          </Accordion>
          <Accordion Accordion title = "Cara Membuat" >
          <Text style={{color:"#383838"}}>1. Potong-potong buncis.</Text>
          <Text style={{color:"#383838"}}>2. Iris bawang merah dan bawang putih. </Text>
          <Text style={{color:"#383838"}}>3. Tumis bawang hingga wangi dan berubah warna. Kemudian masukkan Cabai besar merah, cabai besar hijau, lengkuas dan udang. Masak dan aduk cepat hingga udang berubah warna. </Text>
          <Text style={{color:"#383838"}}>4. Tambahkan buncis, 4 sdm kecap manis dan tempe goreng. </Text>
          <Text style={{color:"#383838"}}>5. 100 ml air, secukupnya garam, kaldu jamur dan gula. Aduk rata  </Text>
          <Text style={{color:"#383838"}}>6. Terakhir irisan daun bawang dan Voilaa jadi dehh! Angkat dan sajikan segera yaa!  </Text>
          </Accordion>
          <Accordion title="Saran">
            <Text style={{color:"#383838"}}>
            Manis enak dan gurih. 😍Buncisnya masih krenyes2 dan tempenya legit meresap manisnya kecap , pas banget jadi temennya si buncis😋. Ini mah udah komplit ya, ada tempe, sayur dan udangnya. Tinggal siapkan nasi putih aja trus Makannn deh! 🍚😋🙆‍♀️
            </Text>
          </Accordion>
        </View>
        <View style={{height: 30}} />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 40,
    marginHorizontal: 24,
    zIndex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  flexRowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
