import React from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  Dimensions,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import SearchBox from '../components/SearchBox';
import Typograpy from '../components/Typograpy';
import {black, generateRgba, white} from '../constants/color';
import {Imgpngfuel, ImgPngFuel4, ImgPngFuel5} from '../constants/images';

export default function Recipes({navigation}) {
  const data = [
    {category: 'Masakan Khas Jawa Timur', img: ImgPngFuel4},
    {category: 'Masakan Khas Jawa Tengah', img: ImgPngFuel4},
    {category: 'Masakan Khas Jawa Barat', img: ImgPngFuel4},
    {category: 'Masakan Khas Betawi', img: ImgPngFuel4},
    {category: 'Masakan Khas Sumatera', img: ImgPngFuel4},
    {category: 'Masakan Khas Kalimantan', img: ImgPngFuel4},
    {category: 'Masakan Khas Sulawesi', img: ImgPngFuel4},
    {category: 'Masakan Khas Bali', img: ImgPngFuel4},
    {category: 'Masakan Khas Nusa Tenggara Barat', img: ImgPngFuel4},
    {category: 'Masakan Khas Papua', img: ImgPngFuel4},
  ];

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: white}}>
      <Typograpy text={'Find Recipe'} style={{textAlign: 'center', color:black}} />
      <SearchBox />
      <FlatList
        keyExtractor={(item, index) => `${index}`}
        numColumns={2}
        contentContainerStyle={{paddingHorizontal: 20}}
        showsVerticalScrollIndicator={false}
        data={data}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate('ExploreDetail')}
              style={{
                flex: 1,
                height: 189.11,
                marginHorizontal: 5,
                marginBottom: 10,
                ...generateRgba(),
                borderWidth: 1,
                borderRadius: 18,
                paddingHorizontal: 40,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                source={item.img}
                style={{width: 93, height: 93, resizeMode: 'contain'}}
              />
              <Text style={{textAlign: 'center', color:black}}>{item.category}</Text>
            </TouchableOpacity>
          );
        }}
      />
    </SafeAreaView>
  );
}
