import React from "react";
import { View, Image } from "react-native";
import { grey } from "../constants/color";
import { ImgLogo } from "../constants/images";
import { useLayoutEffect } from "react";

export default function SPlashScreen({navigation}){
    useLayoutEffect(() => {
        setTimeout(() => {
        navigation.navigate('OnBoarding');
    }, 500);
    }, []);

    return (
    <View
    style={{
        flex:1,
        backgroundColor:grey,
        alignItems:'center',
        justifyContent:'center',
    }}>
        <Image source={ImgLogo} style={{height:68, resizeMode:'center'}} />
        
    </View>
    );
}