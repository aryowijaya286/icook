import React from "react";
import { View, Image, ImageBackground, Text, StyleSheet } from "react-native";
import Button from "../components/Button";
import { green, grey, white } from "../constants/color";
import { ImgLogo, ImgOnboarding, ImgSS } from "../constants/images";

export default function OnBoardingScreen({navigation}){
    return (
    <View style={{flex: 1}}>
        <ImageBackground
        source={ImgOnboarding}
        style={{flex:1, justifyContent:'flex-end', alignItems:'center' }}>
        <Image source={ImgSS} 
        style={{height: 56, resizeMode: 'contain', marginBottom: 35}}/>

        <Text style={styles.txtStyle}>Selamat Datang</Text>
        <Text style={styles.txtStyle}>Di ICook</Text>
        <Text style={{color:white, fontSize:16, marginTop:19, marginBottom:40}}>Disinilah Resep Masakan Anda Bakal Teratasi</Text>

        <Button 
        onPress={() => navigation.navigate('HomeScreen')}
        label={"Get Started"} 
        btnStyle={{backgroundColor:green, width:'80%', marginBottom:40 }} />

        </ImageBackground>
    </View>
    );
}


const styles = StyleSheet.create({
    txtStyle: {fontSize:28, fontWeight:'600', color:white}
})