import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ListRecipe from '../components/ListRecipe';
import SearchBox from '../components/SearchBox';
import Swipper from '../components/Swipper';
import {black, grey_light, white} from '../constants/color';
import {ImgLogin} from '../constants/images'
import { StatusBar } from 'react-native';


export default function HomeScreen(props) {
  return (
    <SafeAreaView style={{backgroundColor: white, flex: 1}}>
      <ScrollView showsVerticalScrollIndicator={false}>
      <StatusBar barStyle={'dark-content'}  backgroundColor="white" />
        <SearchBox />
        <Swipper />

        <ListRecipe title={'Exclusive Offer'} {...props} />
        <ListRecipe title={'Best Selling'} {...props} />
        <View style={{height: 20}} />
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  flexRowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  locationStyle: {
    marginTop: 10,
    justifyContent: 'center',
  },
});
